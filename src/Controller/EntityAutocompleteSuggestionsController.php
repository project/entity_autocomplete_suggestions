<?php

namespace Drupal\entity_autocomplete_suggestions\Controller;

use Drupal\system\Controller\EntityAutocompleteController;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\entity_autocomplete_suggestions\EntityAutocompleteSuggestionsMatcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class to extend EntityAutocompleteController to alter custom list.
 */
class EntityAutocompleteSuggestionsController extends EntityAutocompleteController {

  /**
   * The autocomplete matcher for entity references.
   *
   * @var string
   */
  protected $matcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityAutocompleteSuggestionsMatcher $matcher, KeyValueStoreInterface $key_value) {
    $this->matcher = $matcher;
    $this->keyValue = $key_value;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_autocomplete_suggestions.autocomplete_matcher'),
      $container->get('keyvalue')->get('entity_autocomplete')
    );
  }

}
