<?php

namespace Drupal\entity_autocomplete_suggestions\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Defines a form that configures entity autocomplete suggestions settings.
 */
class AutocompleteSuggestionsConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_autocomplete_suggestions_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_autocomplete_suggestions.settings',
    ];
  }

  /**
   * Constructs a SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory onject.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get autocomplete suggestions settings value.
    $config = $this->configFactory->get('entity_autocomplete_suggestions.settings');
    $form['disallow_unpublished_content'] = [
      '#type' => 'checkbox',
      '#default_value' => (!empty($config->get('disallow_unpublished_content'))) ? $config->get('disallow_unpublished_content') : '',
      '#title' => $this->t('Disallow Unpublished Content'),
    ];
    $form['allow_entity_type'] = [
      '#type' => 'checkbox',
      '#default_value' => (!empty($config->get('allow_entity_type'))) ? $config->get('allow_entity_type') : '',
      '#title' => $this->t('Allow Entity Type'),
    ];
    $form['show_status'] = [
      '#type' => 'checkbox',
      '#default_value' => (!empty($config->get('show_status'))) ? $config->get('show_status') : '',
      '#title' => $this->t('Show Entity Status'),
    ];
    $form['limit_results'] = [
      '#type' => 'textfield',
      '#default_value' => (!empty($config->get('limit_results'))) ? $config->get('limit_results') : '',
      '#title' => $this->t('Autocomplete Suggestions Limit'),
      '#size' => 10,
      '#maxlength' => 2,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('entity_autocomplete_suggestions.settings');

    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value)->save();
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $limit = $form_state->getValue('limit_results');
    if (!is_numeric($limit)) {
      $form_state->setErrorByName('limit_results', $this->t('Limit should be numeric.'));
    }

    parent::validateForm($form, $form_state);
  }

}
