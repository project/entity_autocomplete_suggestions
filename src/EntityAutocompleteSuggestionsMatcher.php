<?php

namespace Drupal\entity_autocomplete_suggestions;

use Drupal\Core\Entity\EntityAutocompleteMatcher;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;

/**
 * Class to alter the autocomplete widget.
 */
class EntityAutocompleteSuggestionsMatcher extends EntityAutocompleteMatcher {

  /**
   * The entity reference selection handler plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected $selectionManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository interface.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepositoryInterface;

  /**
   * Constructs a EntityAutocompleteMatcher object.
   *
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_manager
   *   The entity reference selection handler plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepositoryInterface
   *   The entity repository interface.
   */
  public function __construct(
    SelectionPluginManagerInterface $selection_manager,
    ConfigFactoryInterface $configFactory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityRepositoryInterface $entityRepositoryInterface
  ) {
    $this->selectionManager = $selection_manager;
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepositoryInterface = $entityRepositoryInterface;
  }

  /**
   * Gets matched labels based on a given search string.
   *
   * @param string $target_type
   *   The ID of the target entity type.
   * @param string $selection_handler
   *   The plugin ID of the entity reference selection handler.
   * @param array $selection_settings
   *   An array of settings that will be passed to the selection handler.
   * @param string $string
   *   (optional) The label of the entity to query by.
   *
   * @return array
   *   An array of matched entity labels, in the format required by the AJAX
   *   autocomplete API (e.g. array('value' => $value, 'label' => $label)).
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the current user doesn't have access to the specified entity.
   *
   * @see \Drupal\system\Controller\EntityAutocompleteController
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {

    $matches = [];
    $options = $selection_settings + [
      'target_type' => $target_type,
      'handler'     => $selection_handler,
    ];
    $handler = $this->selectionManager->getInstance($options);

    // Get entity autocomplete suggestions settings.
    $config = $this->configFactory->get('entity_autocomplete_suggestions.settings');

    if (isset($string)) {
      // Limit variable to limit the autocomplete suggestions lists.
      $limit = $config->get('limit_results') ? $config->get('limit_results') : 10;
      // Get an array of matching entities.
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $entity_labels = $handler->getReferenceableEntities($string, $match_operator, $limit);

      // Loop through the entities and convert them into autocomplete output.
      foreach ($entity_labels as $values) {
        foreach ($values as $entity_id => $label) {
          $entity = $this->entityTypeManager->getStorage($target_type)->load($entity_id);
          $entity = $this->entityRepositoryInterface->getTranslationFromContext($entity);

          $type = !empty($entity->type->entity) ? $entity->type->entity->label() : $entity->bundle();
          // Check entity type allow status.
          if (!$config->get('allow_entity_type')) {
            $type = '';
          }

          // To check enity type use $entity->getEntityType()->id() is equal
          // to node or taxonomy_term.
          // Below line is for all entity.
          if (method_exists($entity, 'isPublished')) {
            $status = ($entity->isPublished()) ? ", Published" : ", Unpublished";
          }
          // Set status empty if show status flag is unchecked.
          if (!method_exists($entity, 'isPublished') || !$config->get('show_status')) {
            $status = '';
          }

          // Below line will show entity title with entity id.
          $key = $label . ' (' . $entity_id . ')';
          // Strip things like starting/trailing white spaces,
          // line breaks and tags.
          $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));
          // Names containing commas or quotes must be wrapped in quotes.
          $key = Tags::encode($key);
          // Below label will appear in result of entity autocomplete field
          // having entity title, entity id, entity type and status.
          $conditional_label = '';
          // If both (type and status) will uncheck from configuration page
          // then conditional label will empty.
          if (!empty($type) || !empty($status)) {
            $conditional_label = ' [' . trim($type . $status, ', ') . ']';
          }
          // Conditional label will append in display variable if it will not
          // empty otherwise it will have only entity id in suggestions.
          $display = $key . $conditional_label;
          // Below condition will execute only if published content
          // is allowed in autocomplete suggestions and entity is published.
          if ($config->get('disallow_unpublished_content')) {
            if (!method_exists($entity, 'isPublished') || $entity->isPublished()) {
              $matches[] = ['value' => $key, 'label' => $display];
            }
          }
          // Below conditions will execute if published and unpublished
          // both content are allowed.
          else {
            $matches[] = ['value' => $key, 'label' => $display];
          }
        }
      }
    }
    // Matches array having all matched results based
    // on condition matched above.
    return $matches;
  }

}
