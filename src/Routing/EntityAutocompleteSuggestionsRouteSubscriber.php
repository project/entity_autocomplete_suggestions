<?php

namespace Drupal\entity_autocomplete_suggestions\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class to extend route subscriber to alter route for autocomplete widget.
 */
class EntityAutocompleteSuggestionsRouteSubscriber extends RouteSubscriberBase {

  /**
   * Class to alter route if route is entity autocomplete.
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('system.entity_autocomplete')) {
      $route->setDefault('_controller', '\Drupal\entity_autocomplete_suggestions\Controller\EntityAutocompleteSuggestionsController::handleAutocomplete');
    }
  }

}
