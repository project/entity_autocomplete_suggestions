Entity Autocomplete Suggestions
-------------------------------

INTRODUCTION:
  This module helps in configuring the entity autocomplete suggestions.
  It provides a configuration form to configure the suggestions like
  1. Allow/disallow Content type to show in autocomplete suggestions.
  2. Allow/disallow published/unpublished status to show in autocomplete
    suggestions.
  3. Show vocabulary name in autocomplete suggestions.
  4. Show vocabulary status in autocomplete suggestions only if drupal >= 8.8
     because published field is introduced in this version.
  5. Allows/disallow to limit the autocomplete suggestions. By default this
     limit is set to 10 suggestions only.

REQUIREMENTS:
  This module requires drupal core modules.
  * Entity
  * Entity reference

INSTALLATION:
  Install the module as you would normally install a contributed Drupal module.
  Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION:
  1. Navigate to Administration > Extend and enable the 
     Entity Autocomplete Suggestions module.
  2. Configure module at admin/config/autocomplete-suggestion-configurations.
  3. Following configurations description below
     a. Disallow Unpublished Content.
     b. Limit autocomplete suggestions limits.
     c. Allow to show entity type like content type, vocabulary name in
       autocomplete suggestions.
     d. Show status allows to show entity published/unpublished status.
  4. After installation, Following default values will be set.
     a. Disallow Unpublished Content : TRUE
     b. Limit autocomplete suggestions : 10
     c. Show entity type : TRUE
     d. Show Status : TRUE
